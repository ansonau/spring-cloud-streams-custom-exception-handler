package com.example.demo;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class AlwaysFailEventHandler {
    private Logger LOGGER = LoggerFactory.getLogger(AlwaysFailEventHandler.class);

    @Bean
    public Function<KStream<String, String>, KStream<String, String>> process() {
        return input -> {
            try {
                return input
                        .peek(((key, value) -> LOGGER.info("k/v:{},{}", key, value)))
                        .map((key, value) ->
                                {
                                    if (value.equals("die")) {
                                        throw new RuntimeException("Die");
                                    }
                                    return new KeyValue<>(value, value);
                                }
                        );
            }catch(Exception ex) {
                LOGGER.error("Inside process.apply(),actually this cannot caught exception thrown by DSL like map()" ,ex.fillInStackTrace());
                return input.filter(((key, value) -> key.equals(key)));
            }
        };
    }


}
