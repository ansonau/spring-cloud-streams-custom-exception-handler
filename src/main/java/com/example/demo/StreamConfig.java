package com.example.demo;


import org.apache.kafka.streams.errors.StreamsUncaughtExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.StreamsBuilderFactoryBeanConfigurer;

@Configuration
public class StreamConfig {
    private Logger LOGGER = LoggerFactory.getLogger(StreamConfig.class);

    @Bean
    public StreamsBuilderFactoryBeanConfigurer StreamsBuilderFactoryBeanConfigurer() {
        return factoryBean -> {
            factoryBean.setKafkaStreamsCustomizer(kafkaStreams -> kafkaStreams.setUncaughtExceptionHandler(exception -> {
                LOGGER.error("Kafka-Streams uncaught exception occurred. Stream will be replaced with new thread");
                return StreamsUncaughtExceptionHandler.StreamThreadExceptionResponse.REPLACE_THREAD;
            }));
        };
    }
}
