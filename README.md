Example to show how to prevent Application shutdown when uncaught exception throw by Kafka stream processor code

To produce a message that always throw runtime exception, try below
kafka-console-producer.bat --broker-list localhost:9092 --topic process-in-0 --property "parse.key=true" --property "key.separator=:"

This input is fine
>k:hello

This input value cause runtime exception being throws by application
>k:die
